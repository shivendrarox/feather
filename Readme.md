# Feather
Feather is a lightweight postman alternative. 

# Goals
1. Lightening Fast
2. Low RAM and CPU Usage
3. Get shit done
4. Have fun
5. Write minimum amount of code

# Limitations
1. I am not great at beautifying the looks.
2. Will not support very old browsers or non standards complaint browsers
3. This is the first time I am learning and doing CSS
4. Never built a production website thought I know enough to get stuff done

# Architecture
1. Website - built using Vanilla HTML, CSS and JS
2. Backend - COWJ as server with Zoomba scripting language to manipulate the 
request. 

# Why not send request locally using fetch?
To not deal with CORS and other limitations of browsers.

# References 
1. COWJ - https://github.com/nmondal/cowj
2. Zoomba - https://gitlab.com/non.est.sacra/zoomba

# Run locally
Assuming a Linux environment,

1. `git clone https://github.com/hemilTheRebel/cowj`
2. `cd cowj`
3. `sudo docker build . -t cowj` Note. On mac, you don't need sudo 
4. `cd ..`
5. `git clone https://gitlab.com/rahem027/feather`
6. `cd feather`
7. `sudo docker run --name feather -v $(pwd):/feather -p 8080:8080 -e PORT=8080 cowj /feather/feather.yaml`

## Note:
1. `-p` has the format `<local port>:<container port>`. i.e. if you
want to run on port 5000, the command would be `-p 5000:8080`
2. `-e PORT=8080` is a required environment variable. You can change this to 
whatever but be sure to update `-p` command's container port as well

## Mac OS changes
Docker commands don't need sudo on MacOS

## Windows changes (not tested but we are not using anything unix specific)
- Docker commands don't need sudo on Windows I believe
- In step 7, replace `$(pwd)` with current directory manually. 
I don't know enough cmd/Powershell to write the appropriate 
command that does this automatically.

## Explanation
Step 1 to 3 build the base docker image. It has no backend logic. 
It is the framework. You can also pull the latest version of cowj from
docker hub here: https://hub.docker.com/repository/docker/rahem027/hemil-cowj/general
As of writing the latest tag is 0.0.2

Step 4 to 6 - we clone feather

Step 7:
- `docker run --name feather` - We tell docker to run an image. We 
give this image a name feather. This allows us to write `docker stop feather`
instead of searching for container id everytime
- `-v $(pwd):/feather` - Mount the current directory (feather directory on 
your local machine) to `/feather` inside the container
- `-p 8080:8080` Expose localhost port 8080 as container port 8080 i.e. if
you receive a request for localhost:8080, forward it to container port 8080.
Format is `-p <local port>:<container port>`
- `-e PORT=8080` Set environment variable PORT inside container to 8080. 
Feather can run on any port. Upon boot, it checks the PORT variable
and starts running on that port. **Note:** this port is port inside the 
container not localhost port.
- `cowj` - This is the name of the image we specified built in Step 3.
- `/feather/feather.yaml` - This is the argument of the docker image not docker.
It specifies which file to run to configure everything. **Note:** This 
is path inside the container. Remember we mounted `$(pwd)` to `/feather`?

# Origin Story
Being a web dev, I wanted something lightning fast and lightweight in terms 
of RAM and CPU usage. Initially I planned  to build a desktop app but the 
state of desktop UI frameworks is a disaster. Eventually I gave in and decided,
I have my web browser open 100% of the time anyway. So, I might as well build a 
web page. But the web page itself should not consume too much RAM or CPU.

# Tech Stack Story
Initially I planned to write a small Flask backend but it was too much work.
Then I decided to give Golang a shot but again, it just didn't feel right.
I thought about it for a moment. What do I really want to build? A proxy. 
That's it! Eureka moment. Being familiar with COWJ, I chose it as it is
extremely fast and Zoomba is a scripting language designed for this stuff.
If you take a look at the code, you will know why