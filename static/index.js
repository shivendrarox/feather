"use strict";

const verbsWithBody = ['PUT', 'POST'];

const verb = {
    current: 'GET',
    onChange: function (newVerb) {
        document.getElementById('verb').value = newVerb;
        this.current = newVerb;
        requestReadOnly.onChange(!verbsWithBody.includes(this.current));
    }
}

const requestReadOnly = {
    current: true,
    onChange: function (newReadOnly) {
        this.current = newReadOnly;

        const body = document.getElementById('body');
        if (newReadOnly) {
            body.setAttribute('readonly', 'readonly');
            requestBody.onChange('');
        } else {
            body.removeAttribute('readonly');
        }
    }
}

const requestBody = {
    current: function () { return document.getElementById('body').value },
    onChange: function (newText) {
        document.getElementById('body').value = newText;
    }
}

const requestPart = {
    body: 'body',
    headers: 'headers',
    queryParams: 'queryParams',
}

const requestPartValues = [requestPart.body, requestPart.headers, requestPart.queryParams];

const updateRequestPart = {
    current: requestPart.body,
    onChange: function (newValue) {
        this.current = newValue;
        const visible = document.getElementById(requestPart[newValue]);
        visible.classList.remove('invisible');

        const invisible = [...requestPartValues];
        invisible
            .filter((value) => value !== newValue)
            .forEach((value) => {
                document.getElementById(value).classList.add('invisible');
            });
    }
}

async function send(e) {
    const form = document.getElementById("sendForm");
    if (!form.checkValidity()) {
        console.log("invalid");
        return;
    }

    e.preventDefault();

    const url = document.getElementById('url').value;
    const body = requestBody.current();

    let options = { method: verb.current };
    if (verbsWithBody.includes(verb.current)) {
        options.body = body;
    }

    const headers = new Headers();
    for (let i = 0; i < headerInputs.rows; i++) {
        const key = headerInputs.inputs.get([i, 0].join(',')).value.trim();
        const value = headerInputs.inputs.get([i, 1].join(',')).value.trim();

        if (key !== '') {
            headers.append(`X-Feather-Header-${key}`, value);
        }
    }

    headers.delete('X-FEATHER-URL');
    headers.append('X-FEATHER-URL', url);

    options.headers = headers;

    const queryParams = new URLSearchParams();

    for (let i = 0; i < queryInputs.rows; i++) {
        const key = queryInputs.inputs.get([i, 0].join(',')).value.trim();
        const value = queryInputs.inputs.get([i, 1].join(',')).value.trim();

        if (key !== '') {
            queryParams.append(key, value);
        }
    }

    try {
        progressIndicator.show();
        const response = await fetch('/proxy?' + queryParams, options);

        const body = await response.text();
        console.log(`status: ${response.status}`);
        console.log(`body: ${body}`);

        document.getElementById('status').innerText = `Status: ${response.status.toString()} ${response.statusText}`;
        document.getElementById('resp').value = body;

        /// set response headers
        const headers = document.getElementById('responseHeaders');

        /**
         * Creating a copy. headers.children is live. If we don't create a copy,
         * it will not delete all elements in the below for loop as deleting
         * elements will change the size of the HTMLCollection.
         *
         * Reference - https://developer.mozilla.org/en-US/docs/Web/API/HTMLCollection
         *
         * "An HTMLCollection in the HTML DOM is live; it is automatically updated when
         * the underlying document is changed. For this reason it is a good idea to make
         * a copy (e.g., using Array.from) to iterate over if adding, moving, or removing nodes."
         */
        const elements = Array.from(headers.children);

        /// First element is the heading. Ignore it.
        for (let i = 1; i < elements.length; i++) {
            elements[i].remove();
        }

        response.headers.forEach((value, key) => {
            /// Only headers starting with this prefix are useful.
            /// This prefix is useful because headers like Set-Cookie
            /// if returned by the proxy, can set a cookie on feather
            /// which can lead to security vulnerabilities
            const prefix = 'x-feather-header-';
            if (!key.startsWith(prefix)) { return; }
            const fragment = document.createDocumentFragment();

            const div = document.createElement('div');
            div.classList.add('horizontal-flex');

            const input1 = document.createElement('input');
            input1.classList.add('control');
            input1.classList.add('cell');

            /**
             * Set-Cookie is a special header. Other headers are required by
             * the standard to be treated as if they were comma separated if
             * they appear multiple times. Set-Cookie is the only exception.
             * So Set-Cookie headers are returned as `x-feather-set-cookie-0`
             * `x-feather-set-cookie-1` and so on. So, if after removing the prefix,
             * if header starts with set-cookie, then header name is set cookie
             * thereby discarding the number. Otherwise, we just strip the prefix
             */
            const header = key.substring(prefix.length);
            input1.value = header.startsWith('set-cookie') ? 'set-cookie' : header ;

            input1.readOnly = true;
            div.appendChild(input1);

            const input2 = document.createElement('input');
            input2.classList.add('control');
            input2.classList.add('cell');
            input2.value = value;
            input1.readOnly = true;
            div.appendChild(input2);

            fragment.appendChild(div);
            headers.appendChild(div);
        });
    } catch (e) {
        console.log(e);
    } finally {
        progressIndicator.hide();
    }
}

/**
 * @typedef Point
 * @type {object}
 * @property {number} x - Zero based value of x
 * @property {number} y - Zero based value of y
 */

/**
 * @typedef EditableTable
 * @type {object}
 * @property {number} rows - Number of rows in the table
 * @property {Map<HTMLInputElement, Point>} locations - Map of input field ==> Point object
 * @property {Map<String, HTMLInputElement>} inputs - Map of x, y co ordinates joined as `${x},${y}` ==> element.
 * Unfortunately, JS does not support list as keys properly. Hence, the hack
 * @property {string} id - ID of the element that refers to EditableTable
 */


/**
 * @type EditableTable
 */
const headerInputs = {
    rows: 1,
    /// mapping of input element to {row:x, col:y}
    locations: new Map(),
    /// mapping of [row, col] to input
    inputs: new Map(),
    id: 'headers',
}

/**
 * @type EditableTable
 */
const queryInputs = {
    rows: 1,
    /// mapping of input element to {row:x, col:y}
    locations: new Map(),
    /// mapping of [row, col] to input
    inputs: new Map(),
    id: 'queryParams',
}

function createInputRow(fragment) {
    const div = document.createElement('div');
    div.classList.add('horizontal-flex');

    const input1 = document.createElement('input');
    input1.classList.add('control');
    input1.classList.add('cell');
    div.appendChild(input1);

    const input2 = document.createElement('input');
    input2.classList.add('control');
    input2.classList.add('cell');
    div.appendChild(input2);

    fragment.appendChild(div);
    return [input1, input2];
}

/**
 * Adds a new row if this function see fits
 * @param {EditableTable} inputs - a header input like object which maintains the inputs and their locations
 * @param {KeyboardEvent} event - an event for keydown
 */
function addNewRowIfLast(inputs, event) {
    const location = inputs.locations.get(event.target);
    if (location === undefined) {
        return;
    }

    if (event.shiftKey) {
        return;
    }

    /// if I am on the last row, add a new one
    if (location.row === inputs.rows - 1) {
        const element = document.getElementById(inputs.id);
        const fragment = document.createDocumentFragment();
        const [input1, input2] = createInputRow(fragment);
        element.appendChild(fragment);

        inputs.rows += 1;
        inputs.locations.set(input1, {row: location.row + 1, col: 0});
        inputs.locations.set(input2, {row: location.row + 1, col: 1});
        /// JS cant use list as a key because === in list compares if they
        /// refer to same object not if they are equal. So converting to string
        /// Reference - https://stackoverflow.com/a/43592969
        inputs.inputs.set([location.row + 1, 0].join(','), input1);
        inputs.inputs.set([location.row + 1, 1].join(','), input2);
    }
}

/**
 * Remove a row if empty
 * @param {EditableTable} inputs - a header input like object which maintains the inputs and their locations
 * @param {FocusEvent} event - an event for keydown
 */
function removeLastRowIfEmpty(inputs, event) {
    const location = inputs.locations.get(event.target);
    if (location === undefined) {
        return;
    }

    const input1 = inputs.inputs.get([location.row, 0].join(','));
    const input2 = inputs.inputs.get([location.row, 1].join(','));

    if (inputs.rows > 1 && input1.value.trim() === '' && input2.value.trim() === '') {
        const headers = document.getElementById(inputs.id);
        const divs = headers.querySelectorAll('div.horizontal-flex');
        /// First row in this list is the header. So, we need to add one
        divs[location.row+1].remove();

        const headerKey = [location.row, 0].join(',');
        const headerValue = [location.row, 1].join(',');

        inputs.inputs.delete(headerKey);
        inputs.inputs.delete(headerValue);
        inputs.locations.delete(input1);
        inputs.locations.delete(input2);

        for (let i = location.row+1; i < inputs.rows; i++) {
            const headerKey = [i, 0].join(',');
            const input1 = inputs.inputs.get(headerKey);
            inputs.inputs.delete(headerKey);
            inputs.inputs.set([i-1, 0].join(','), input1);
            inputs.locations.set(input1, { row: i-1, col: 0 });

            const headerValue = [i, 1].join(',');
            const input2 = inputs.inputs.get(headerValue);
            inputs.inputs.delete(headerValue);
            inputs.inputs.set([i-1, 1].join(','), input2);
            inputs.locations.set(input2, { row: i-1, col: 1 });
        }

        inputs.rows -= 1;
    }
}

const showResponseBody = {
    current: true,
    onChange: function (newValue) {
        this.current = newValue;
        const headers = document.getElementById('responseHeaders');
        const body = document.getElementById('resp');
        if (newValue) {
            headers.classList.add('invisible');
            body.classList.remove('invisible');
        } else {
            headers.classList.remove('invisible');
            body.classList.add('invisible');
        }
    }
};

const url = {
    current: null,
    onChange: function (newValue) {
        const now = Date.now();
        this.current = new URL(newValue);
        console.log(`url created. time taken: ${Date.now() - now}`);

        updateQueryParameters();
    },
};

function updateQueryParameters() {
    const now = Date.now();

    /// copying into array to allow index based lookup
    const searchParams = Array.from(url.current.searchParams);
    const queryParamCount = searchParams.length;

    // for (let i = 0; i < Math.min(queryInputs.rows, queryParamCount); i++) {
    //     const param = queryInputs.inputs.get([i, 0].join(','));
    //     param.value = searchParams[i][0];
    //
    //     const value = queryInputs.inputs.get([i, 1].join(','));
    //     value.value = searchParams[i][1];
    // }

    /**
     * We want to ensure there are parameter count + 1 rows at the end
     * of this function. One row extra for to add a new query param if any
     * At this point, there are 3 possibilities
     * 1. queryInputs.rows < queryParameterCount + 1 - add ((queryParameterCount + 1) - queryInputs.row) rows
     * 2. queryInputs.rows == queryParameterCount + 1 - do nothing
     * 2. queryInputs.rows > queryParameterCount - Remove (queryInputs.rows - (queryParameterCount + 1)) rows
     */

    if (queryInputs.rows < queryParamCount + 1) {
        const element = document.getElementById(queryInputs.id);
        const fragment = document.createDocumentFragment();

        for (let i = queryInputs.rows; i < queryParamCount + 1; i++) {
            const [input1, input2] = createInputRow(fragment);
            element.appendChild(fragment);

            queryInputs.locations.set(input1, {row: i, col: 0});
            queryInputs.locations.set(input2, {row: i, col: 1});

            queryInputs.inputs.set([i, 0].join(','), input1);
            queryInputs.inputs.set([i, 1].join(','), input2);
        }
    } else if (queryInputs.rows > queryParamCount + 1) {
        /// index start from 0. Hence the (- 1)
        for (let i = queryInputs.rows - 1; i > queryParamCount; i--) {
            const keyString = [i, 0].join(',');
            const key = queryInputs.inputs.get(keyString);
            key.remove();
            queryInputs.inputs.delete(keyString);
            queryInputs.locations.delete(key);

            const valueString = [i, 1].join(',');
            const value = queryInputs.inputs.get(valueString);
            value.remove();
            queryInputs.locations.delete(valueString);
            queryInputs.locations.delete(value);
        }
    }

    /// at this point, rows should be queryParamCount + 1
    queryInputs.rows = queryParamCount + 1;

    for (let i = 0; i < queryParamCount; i++) {
        const key = queryInputs.inputs.get([i, 0].join(','));
        key.value = searchParams[i][0];

        const value = queryInputs.inputs.get([i, 1].join(','));
        value.value = searchParams[i][1];
    }

    /// Make sure last row is empty
    queryInputs.inputs.get([queryParamCount, 0].join(',')).value = '';
    queryInputs.inputs.get([queryParamCount, 1].join(',')).value = '';

    console.log(`query params updated. time taken: ${Date.now() - now}`);
}

function reset() {
    const fixedInputs = ['url', 'body', 'resp'];
    for (const input of fixedInputs) {
        document.getElementById(input).value = '';
    }

    for (const input of headerInputs.locations.keys()) {
        input.value = '';
    }

    for (const input of queryInputs.locations.keys()) {
        input.value = '';
    }
}

const progressIndicator = {
    showing: false,
    show: function () {
        this.showing = true;
        document.getElementById('progressIndicator').classList.remove('invisible');
    },
    hide: function() {
        this.showing = false;
        document.getElementById('progressIndicator').classList.add('invisible');
    }
}

window.onload = () => {
    const form = document.getElementById("sendForm");
    form.onsubmit = send;

    const verbElement = document.getElementById("verb");
    verbElement.onchange = (e) => verb.onChange(e.target.value);

    verb.onChange(verbElement.selectedOptions[0].value);

    const urlElement = document.getElementById('url');
    urlElement.onchange= (e) => url.onChange(e.target.value);

    /// initialize headerInputs
    const inputs = document.querySelectorAll('#headers input');
    inputs.forEach(function (input, index) {
        headerInputs.locations.set(input, { row: 0, col: index });
        headerInputs.inputs.set([0, index].join(','), input);
    });

    console.log(headerInputs);

    const headers = document.getElementById('headers');
    headers.addEventListener('focusout', (event) => removeLastRowIfEmpty(headerInputs, event));
    headers.addEventListener('keydown', (event) => addNewRowIfLast(headerInputs, event));

    /// initialize queryParams
    const queryParamInputs = document.querySelectorAll('#queryParams input');
    queryParamInputs.forEach(function (input, index) {
        queryInputs.locations.set(input, { row: 0, col: index });
        queryInputs.inputs.set([0, index].join(','), input);
    });

    console.log(queryParamInputs);

    const queryParams = document.getElementById('queryParams');
    queryParams.addEventListener('focusout', (event) => removeLastRowIfEmpty(queryInputs, event));
    queryParams.addEventListener('keydown', (event) => addNewRowIfLast(queryInputs, event));

    /// Firefox seems to remember the values of some inputs across refresh.
    /// Reset all inputs to their blank state
    reset();
}